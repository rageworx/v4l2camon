# Makefile for V4L2 camera monitoring service daemon
# ----------------------------------------------------------------------
# 
# (C) 2019 Raphael Kim
#

# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

# Compiler definitions
GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = $(CC_PATH)/ar
FCG = fltk-config --use-images

# FLTK configs 
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# Base PATH
BASE_PATH = .
SRC_PATH  = $(BASE_PATH)/src

# INSTALLATION pathes
INST_PATH = /usr/local/bin
DESK_PATH = /usr/share/applications
IRES_PATH = /usr/local/share/icons/hicolor/256x256/apps

# External sources
FITK_PATH = ../fl_imgtk
FITK_I    = $(FITK_PATH)/lib
FITK_L    = $(FITK_PATH)/lib
ENDE_PATH = ../libendetool
ENDE_I    = $(ENDE_PATH)/src
ENDE_L    = $(ENDE_PATH)/lib
V4LT_PATH = ../v4l-utils
V4LT_I    = $(V4LT_PATH)/include
V4LT_U    = $(V4LT_PATH)/utils
V4LT_C    = $(V4LT_U)/common

# TARGET settings
TARGET_PKG = v4l2camon
TARGET_DIR = ./bin
TARGET_OBJ = ./obj

# DEFINITIONS
DEFS += -D_POSIX_THREADS -DMININI_ANSI
DEFS += -DUSE_OMP

# Compiler optiops 
#COPTS += -std=c++11
COPTS += -fexceptions -O3 -s
COPTS += -ffast-math -fopenmp

# CC FLAG
CFLAGS += -I$(SRC_PATH)
CFLAGS += -Ires -Ilib
CFLAGS += -I$(ENDE_I)
CFLAGS += -I$(V4LT_I) -I$(V4LT_U) -I$(V4LT_C)
CFLAGS += -I$(FITK_I)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)
CFLAGS += $(FLTKCFG_CXX)

# LINK FLAG
LFLAGS += -mtune=native
LFLAGS += -L$(ENDE_L)
LFLAGS += -L$(FITK_L)
LFLAGS += -lpthread
LFLAGS += -lfl_imgtk
LFLAGS += $(FLTKCFG_LFG)
LFLAGS += -lturbojpeg

# Sources
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)

.PHONY: prepare clean continue

all: prepare continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Compiling $< to $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS)
	@echo "Generating(Linking) $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."

install: $(TARGET_DIR)/$(TARGET_PKG)
	@mkdir -p $(IRES_PATH)
	@cp -rf $< $(INST_PATH)
	@cp -rf res/$(TARGET_PKG).png $(IRES_PATH)
	@./mkinst.sh $(INST_PATH) $(DESK_PATH)

uninstall: $(INST_PATH)/$(TARGET_PKG)
	@rm -rf $<
	@rm -rf $(IRES_PATH)/$(TARGET_PKG).png
	@rm -rf $(DESK_PATH)/$(TARGET_PKG).desktop

