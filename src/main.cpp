#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include <cstdio>
#include <cstdlib>
#include <string>

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>

#include <v4l-helpers.h>
#include <fl_imgtk.h>
#include <turbojpeg.h>

#include "cam_v4l2.h"
#include "yuvconv.h"
#include "rgb16conv.h"
#include "mjpgconv.h"
#include "resource.h"

//////////////////////////////////////////////////////////////////////////
//
// V4L2 camera monitoring application,
// --------------------------------------------------------------------
// (C)2019 Raphael Kim, rageworx@gmail.com
//
//////////////////////////////////////////////////////////////////////////

using namespace std;

//////////////////////////////////////////////////////////////////////////

static char wintitle[80] = {0};
static CamV4L2* cam;
static char strMe[128] = {0};
static char strInfo[1024] = "No Info";
static char strSavePath[512] = {0};

static Fl_Double_Window* window = NULL;
static Fl_Box* boxView = NULL;
static Fl_Box* boxInfo = NULL;
static Fl_Button* btnCapture = NULL;
static Fl_RGB_Image* imgView = NULL;
static Fl_RGB_Image* winIcon = NULL;

static pthread_t ptt;
static bool looping = true;
static bool save2file = false;
static int  lockpair = 0;

static bool opt_socksvrmode = false;
static bool opt_showhelp = false;
static bool opt_configload = false;
static char opt_configpath[512] = {0};

//////////////////////////////////////////////////////////////////////////

// write to JPEG using turbojpeg !
bool save2jpg( Fl_RGB_Image* img, const char* fname, int q )
{
    bool retb = false;

    if ( ( img != NULL ) && ( fname != NULL ) )
    {
        if ( img->d() == 2 ) /// RGB565,555 not supported.
            return false;

        int w = img->w();
        int h = img->h();
        int d = img->d();
        int jfmt = 0;
        int jsmp = 0;

        tjhandle jComp = tjInitCompress();

        if ( jComp != 0 )
        {
            switch( d )
            {
                case 1: /// gray.
                    jfmt = TJPF_GRAY;
                    jsmp = TJSAMP_GRAY;
                    break;

                case 3: /// RGB
                    jfmt = TJPF_RGB;
                    jsmp = TJSAMP_444;
                    break;

                case 4: /// RGBA
                    jfmt = TJPF_RGBA;
                    jsmp = TJSAMP_444;
                    break;
            }

            const unsigned char* refa = \
                (const unsigned char*)img->data()[0];
            
            unsigned char*      outb = NULL;
            long unsigned int   outsz = 0;

            tjCompress2( jComp, refa, w, 0, h, jfmt,
                         &outb, &outsz, jsmp, q,
                         TJFLAG_FASTDCT );
            tjDestroy( jComp );

            if ( ( outb != NULL ) && ( outsz > 0 ) )
            {
                FILE* fp = fopen( fname, "wb" );
                if ( fp != NULL )
                {
                    fwrite( outb, 1, outsz, fp );
                    fclose( fp );

                    retb = true;
                }

                delete[] outb;
                outsz = 0;
            }
        }
    }

    return retb;
}

void gentimestr( char* strmap )
{
    if( strmap == NULL )
        return;

    time_t rtime;
    time( &rtime );
    struct tm* tinfo = localtime( &rtime );

    timeval tv;
    gettimeofday(&tv, NULL);

    sprintf( strmap, 
             "v4l2camon-%04d-%02d-%02d-%02d-%02d-%02d-%03d",
             tinfo->tm_year + 1900,
             tinfo->tm_mon + 1,
             tinfo->tm_mday,
             tinfo->tm_hour,
             tinfo->tm_min,
             tinfo->tm_sec,
             tv.tv_usec % 1000 );
}

void flwcb( Fl_Widget* w, void* p )
{
    if ( w == window )
    {
        looping = false;
        pthread_cancel( ptt );
        // unlock FLTK
        if ( lockpair > 0 )
        {
            Fl::unlock();
            lockpair--;
        }
        pthread_join( ptt, NULL );

        window->hide();
        return;
    }

    if ( w == btnCapture )
    {
        if ( save2file == false )
        {
            btnCapture->deactivate();
            save2file = true;
        }
    }
}

Fl_RGB_Image* loadIcon( Fl_Double_Window* w )
{
    if ( w == NULL )
        return NULL;

    string iconfn  = "/v4l2camon.png";
    string trypath = "/usr/local/share/icons/hicolor/256x256/apps";
    string fpath = trypath + iconfn;
    bool   existed = false;

    if ( access( fpath.c_str() , 0 ) == 0 )
    {
        existed = true;
    }
    else
    {
        trypath = getenv( "PWD" );
        fpath = trypath + "/res " + iconfn;

        if ( access( fpath.c_str(), 0 ) == 0 )
        {
            existed = true;
        }
    }

    if ( existed == true )
    {
        Fl_RGB_Image* imgIcon = new Fl_PNG_Image( fpath.c_str() );
        if ( imgIcon != NULL )
        {
            w->icon( imgIcon );
            return imgIcon;
        }
    }

    return NULL;
}

void create_components()
{
    int v_arr[] = {APP_VERSION};
    
    sprintf( wintitle,
             "FLTK V4L2 CAMon, v. %d.%d.%d.%d",
             v_arr[0], v_arr[1], v_arr[2], v_arr[3] );

    window = new Fl_Double_Window( cam->width(),
                                   cam->height(),
                                   wintitle );
    if ( window != NULL )
    {
        winIcon = loadIcon( window );
        if ( winIcon != NULL )
        {
            window->icon( winIcon );
        }

        boxView = new Fl_Box( 0, 0,
                              window->w(), window->h() );
        if ( boxView != NULL )
        {
            boxView->box( FL_FLAT_BOX );
            boxView->color( 0 );
        }

        boxInfo = new Fl_Box( 0, 0,
                              window->w(), window->h() / 3,
                              strInfo );
        if ( boxInfo != NULL )
        {
            boxInfo->box( FL_NO_BOX );
            boxInfo->labelcolor( FL_RED );
            boxInfo->labelfont( FL_COURIER );
            boxInfo->labelsize( 10 );
            boxInfo->labeltype( FL_EMBOSSED_LABEL );
            boxInfo->align( FL_ALIGN_INSIDE | FL_ALIGN_TOP_LEFT );
        }

        btnCapture = new Fl_Button( 0, window->h() - 30,
                                    window->w(), 30, "@circle" );
        if ( btnCapture != NULL )
        {
            btnCapture->box( FL_NO_BOX );
            btnCapture->labelsize( 20 );
            btnCapture->labelcolor( FL_RED );
            btnCapture->clear_visible_focus();
            btnCapture->callback( flwcb, NULL );
        }

        window->callback( flwcb, NULL );
        window->show();
    }
}

void* ptloop( void* p )
{
    unsigned prevts = 0;
    const char* sfmt = cam->textformat();
    unsigned curpfmt = cam->format();
    bool bfailurecase = false;

    if ( cam != NULL )
    {
        cam->poll();

        if ( cam->polling() == true )
        {
            // waste some buffers ..
            cam->wastebuffer( 10 );

            unsigned cam_w = cam->width();
            unsigned cam_h = cam->height();

            uchar* cambuff = NULL;

            while( looping == true )
            {
                unsigned bsz = 0;
                uchar*   bsrc = NULL;
                unsigned bts = 0;
                unsigned rsz = 0;

                if ( cam->grabindir( true, bsz, bsrc, &bts ) == true )
                {
                    if ( ( bsz > 0 ) && ( bts > 0 ) )
                    {
                        switch( curpfmt )
                        {
                            case V4L2_PIX_FMT_YUYV:
                            {
                                rsz = \
                                yuyv2rgb( bsrc, bsz, 
                                          (void**)&cambuff, 
                                          cam_w, cam_h );
                            }
                            break;

                            case V4L2_PIX_FMT_UYVY:
                            {
                                rsz = \
                                uyvy2rgb( bsrc, bsz,
                                          (void**)&cambuff,
                                          cam_w, cam_h );
                            }
                            break;

                            case V4L2_PIX_FMT_MJPEG:
                            {
                                rsz = \
                                mjpeg2rgb( bsrc, bsz,
                                           (void**)&cambuff,
                                           cam_w, cam_h );
                            }
                            break;

                            case V4L2_PIX_FMT_RGB555:
                            {
                                rsz = \
                                rgb555rgb( bsrc, bsz,
                                           (void**)&cambuff,
                                           cam_w, cam_h );
                            }
                            break;

                            case V4L2_PIX_FMT_RGB565:
                            {
                                rsz = \
                                rgb565rgb( bsrc, bsz,
                                           (void**)&cambuff,
                                           cam_w, cam_h );
                            }
                            break;
                        }
                    }
                }

                delete[] bsrc;

                if ( ( cambuff != NULL ) && ( rsz > 0 ) )
                {
                    lockpair++;
                    Fl::lock();
                    boxView->image( NULL );
                    if ( imgView != NULL )
                    {
                        fl_imgtk::discard_user_rgb_image( imgView );
                    }

                    imgView = new Fl_RGB_Image( cambuff, 
                                                cam_w, cam_h, 3 );
                    boxView->image( imgView );
                    boxView->redraw();

                    if ( prevts > 0 )
                    {
                        unsigned tdiff = bts - prevts;
                        float    fps = 1000.f / (float)tdiff;

                        sprintf( strInfo,
                                 "%u x %u [%s], %.2f fps",
                                 cam_w, cam_h, sfmt, fps );
                        boxInfo->redraw();
                    }

                    prevts = bts;

                    if ( save2file == true )
                    {
                        save2file = false;
                        char timestr[64] = {0};
                        char writepath[512] = {0};
                        gentimestr( timestr );
                        sprintf( writepath, "%s/%s.jpg",
                                 strSavePath, timestr );
                        save2jpg( imgView, writepath, 91 );
                        btnCapture->activate();
                    }

                    btnCapture->redraw();

                    Fl::unlock();
                    lockpair--;
                    Fl::awake();
                }

                usleep( 33 * 1000 ); /// maximum 30fps.
            }

            delete[] cambuff;
        }
        else
        {
            // need to quit.
            bfailurecase = true;
        }

        cam->stop();
    }

    if ( bfailurecase == true )
    {
        window->do_callback();
    }

    pthread_exit( NULL );
    return NULL;
}

void releaseres()
{
    fl_imgtk::discard_user_rgb_image( imgView );
    fl_imgtk::discard_user_rgb_image( winIcon );
}

void showusage()
{
    printf( "\n" );
    printf( "\t%s (options)\n" );
    printf( "\n" );
    printf( "\t_options_\n" );
    printf( "\t    -h        : show help in stdout and quit.\n" );
    printf( "\t    -s        : socket server mode ( no GUI ).\n" );
    printf( "\t    -c [path] : configuration path if server mode.\n" );
    printf( "\n" );
}

void parseargs( int argc, char** argv )
{
    string strtmp;

    // check myself.
    strtmp = argv[0];
    size_t fpos = strtmp.find_last_of( '/' );
    if ( fpos != string::npos )
    {
        strtmp = strtmp.substr( fpos + 1 );
    }

    strncpy( strMe, strtmp.c_str(), 128 );

    // parse others.

    for( int cnt=1; cnt<argc; cnt++ )
    {
        strtmp = argv[cnt];

        if ( strtmp.find( "-h" ) == 0 )
        {
            opt_showhelp = true;
        }
        else
        if ( strtmp.find( "-s" ) == 0 )
        {
            opt_socksvrmode = true;
        }
        else
        if ( strtmp.find( "-c" ) == 0 )
        {
            opt_configload = true;
        }
        else
        {
            if ( ( opt_configload == true ) 
                 && ( strlen( opt_configpath ) == 0 ) )
            {
                strncpy( opt_configpath, strtmp.c_str(), 512 ) ;
            }
        }
    }
}

void sighandle( int sig )
{
    static bool sh_mutex = false;

    if ( sh_mutex == false )
    {
        sh_mutex = true;

        // close FLTK window.
        if ( window != NULL )
        {
            window->do_callback();
        }

        // check thread still alive ?
        if ( looping == true )
        {
            looping = false;
        }

        int ecode = pthread_kill( ptt, 0 );
        if ( ecode != 0 )
        {
            // wait for thread ends.
            pthread_join( ptt, NULL );
        }

        releaseres();
        exit( 0 );
    }
}

int main( int argc, char** argv )
{
    int reti = 0;

    parseargs( argc, argv );

    if ( opt_showhelp == true )
    {
        showusage();
        return 0;
    }

    // resgister signal handler ..
    signal( SIGINT, sighandle );

    // FLTK setting up
    if ( opt_socksvrmode == false )
    {
        Fl::lock(); /// FLTK thread lock initialize.
        Fl::scheme( "flat" );
    }

    sprintf( strSavePath, "%s/Pictures", getenv( "HOME" ) );
    if ( access( strSavePath, 0 ) != 0 )
    {
        mkdir( strSavePath, 666 );
    }

    cam = new CamV4L2( 0 );
    if ( cam != NULL )
    {
        if ( opt_socksvrmode == false )
        {
            create_components();
        }

        int ptr = pthread_create( &ptt, NULL, ptloop, NULL );

        if ( ptr == 0 )
        {
            reti = Fl::run();
        }

        pthread_join( ptt, NULL );
        releaseres();
        delete cam;
    }

    return reti;
}
