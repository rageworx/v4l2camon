#ifndef __CAM_V4L2_H__
#define __CAM_V4L2_H__

/**************************************************************************
** Cam_V4L2,
** -----------------------------------------------------------------------
** (C)2019 Raphael Kim,
**
** A mini port V4L2 wrapping class for some cameras.
**
**************************************************************************/

class CamEvent
{
    public:
        enum{
            CSIG_NONE = 0,
            CSIG_OPEN,
            CSIG_CLOSE,
            CSIG_POLLING,
            CSIG_MAX
        }ControlSigs;

        virtual void controlled( unsigned sig, void* param ) = 0;
};

class CamV4L2
{
    public:
        CamV4L2( int deviceID, CamEvent* event = NULL );
        ~CamV4L2();

    public:
        const char* textinfo();
        void        poll();
        bool        polling() { return state_poll; }
        void        stop();
        unsigned    width();
        unsigned    height();
        unsigned    depth();
        const char* textformat();
        unsigned    format();
        void        gain( unsigned v );
        unsigned    gain();
        void        gainrange( unsigned &minv, unsigned &maxv );
        void        exposure( unsigned v );
        unsigned    exposure();
        void        exposurerange( unsigned &minv, unsigned &maxv );
        float       temperature();
        bool        framerate( float fps );
        float       framerate();

    public: /// about grab, buffers.
        bool        grabindir( bool waitsync, unsigned &bufflen, unsigned char* &buff, unsigned* ts = NULL );
        void        wastebuffer( unsigned frames = 1 );

    public:
        const char* lasterror();

    public:
        static void dev_fd_div( int n );
        static int  dev_fd_div();

    public:
        const char* mapped_dev_path()   { return str_devmap; }
        const char* mapped_subdev_path(){ return str_subdevmap; }

    protected:
        void opendevice();
        void closedevice();
        void get_ext_controls( void* p );
        void map_devicefd();
        void clear_errors();
        void remove_ext_controls();
        bool allocateBuffers();
        bool deallocateBuffers();

    private:
        int         id_req_dev;
        int         idx_cfd;
        char        str_devmap[128];
        char        str_subdevmap[128];
        char        str_error[1024];
        char        str_devinfo[1024];
        int         id_error;
        unsigned    img_w;
        unsigned    img_h;
        unsigned    img_d;
        unsigned    img_p;
        unsigned    img_f;
        bool        state_poll;
        CamEvent*   eventhandler;
};

#endif /// of __CAM_V4L2_H__
