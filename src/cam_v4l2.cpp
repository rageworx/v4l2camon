#ifndef _WIN32
#include <unistd.h>
#include <signal.h>

#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <fcntl.h>
#include <cstring>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <poll.h>

#include <v4l-helpers.h>
#include "cam_v4l2.h"

#include <vector>

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// -- about REQ_BUFF_SZ, --------------------------------------------------
// This buffer size is depends on real hardware what you embedded,
// Some high-preformance MIPI CSI2 cameras may need to increase buffer size
// up to 32 in maximum.
#define REQ_BUFF_SZ             4
// ------------------------------------------------------------------------

// -- about WASTEBUFFER_SYNC ----------------------------------------------
// This flag may used in wasting buffer.
// ------------------------------------------------------------------------
#define WASTEBUFFER_SYNC

typedef struct _caminfo
{
    v4l_fd                      cfd;
    int                         sfd;
    struct v4l2_query_ext_ctrl* extCtlExposure;
    struct v4l2_query_ext_ctrl* extCtlGain;
    v4l_buffer                  vbuffer;
    v4l_queue                   vqueue;
    unsigned char*              bufferGrab;
    CamV4L2*                    controller;
}caminfo;

////////////////////////////////////////////////////////////////////////////////

static caminfo* camlist[32] = {NULL};

////////////////////////////////////////////////////////////////////////////////

const char* fmt2str( unsigned fmt )
{
    static char retstr[5] = {0};
    memset( retstr, 0, 5 );

    retstr[0] = fmt & 0xFF;
    retstr[1] = (fmt >> 8) & 0xFF;
    retstr[2] = (fmt >> 16) & 0xFF;
    retstr[3] = (fmt >> 24) & 0xFF;

    return retstr;
}

////////////////////////////////////////////////////////////////////////////////

static int idx_fddiv = 5;

void CamV4L2::dev_fd_div( int n )
{
   if ( ( n > 0 ) && ( n != idx_fddiv ) )
   {
       idx_fddiv = n;
   }
}

int CamV4L2::dev_fd_div()
{
    return idx_fddiv;
}

////////////////////////////////////////////////////////////////////////////////

CamV4L2::CamV4L2( int deviceID, CamEvent* event )
 : id_req_dev( deviceID ),
   idx_cfd( -1 ),
   state_poll( false ),
   id_error( 0 ),
   img_w(0),
   img_h(0),
   img_d(2),
   eventhandler( event )
{
    clear_errors();
    opendevice();
}

CamV4L2::~CamV4L2()
{
    closedevice();
}

const char* CamV4L2::textinfo()
{
    memset( str_devinfo, 0, 1024 );

    if ( idx_cfd >= 0 )
    {
        sprintf( str_devinfo,
                 "DEV_PATH=%s;"
                 "SUB_PATH=%s;"
                 "WIDTH=%u;"
                 "HEIGHT=%u;"
                 "DEPTH=%u;"
                 "FORMAT=%s;",
                 str_devmap,
                 str_subdevmap,
                 img_w,
                 img_h,
                 img_d,
                 fmt2str( img_f ) );
    }
    else
    {
        sprintf( str_devinfo, "device not open yet" );
    }

    return str_devinfo;
}

void CamV4L2::poll()
{
    if ( idx_cfd < 0 )
        return;

    if ( state_poll == false )
    {
        deallocateBuffers();

        bool result = allocateBuffers();
        // make stream on !
        int ret = v4l_ioctl( &camlist[idx_cfd]->cfd,
                             VIDIOC_STREAMON,
                             &camlist[idx_cfd]->vqueue.type );

        if ( ret < 0 )
        {
            sprintf( str_error,
                     "V4L ioctrl VIDIOC_STREAMON failure" );
        }
        else
        {
            state_poll = true;
            if ( eventhandler != NULL )
            {
                eventhandler->controlled( CamEvent::CSIG_POLLING,
                                          &state_poll );
            }
        }
    }
}

void CamV4L2::stop()
{
    if ( idx_cfd < 0 )
        return;

    if ( state_poll == true )
    {
        int ret = v4l_ioctl( &camlist[idx_cfd]->cfd,
                             VIDIOC_STREAMOFF,
                             &camlist[idx_cfd]->vqueue.type );
        if ( ret < 0 )
        {
            sprintf( str_error,
                     "V4L ioctrl VIDIOC_STREAMOFF failure" );
        }
        else
        {
            state_poll = false;

            if ( eventhandler != NULL )
            {
                eventhandler->controlled( CamEvent::CSIG_POLLING,
                                          &state_poll );
            }
        }
    }
}

unsigned CamV4L2::width()
{
    if ( idx_cfd >= 0 )
    {
        return img_w;
    }

    return 0;
}

unsigned CamV4L2::height()
{
    if ( idx_cfd >= 0 )
    {
        return img_h;
    }

    return 0;
}

unsigned CamV4L2::depth()
{
    if ( idx_cfd >= 0 )
    {
        return img_d;
    }

    return 0;
}

const char* CamV4L2::textformat()
{
    if ( idx_cfd >= 0 )
    {
        return fmt2str( img_f );
    }

    return NULL;
}

unsigned CamV4L2::format()
{
    if( idx_cfd >= 0 )
    {
        return img_f;
    }

    return 0;
}

bool CamV4L2::grabindir( bool waitsync, unsigned &bufflen, unsigned char* &buff, unsigned* ts )
{
    if ( idx_cfd >= 0 )
    {
        if ( state_poll == false )
        {
            id_error = -1010;
            sprintf( str_error,
                     "device not stated at polling yet" );

            return false;
        }

        if ( waitsync == true )
        {
            fd_set fds;
            struct timeval tv;

            FD_ZERO( &fds );
            FD_SET( camlist[idx_cfd]->cfd.fd, &fds );
            tv.tv_sec = 1;
            tv.tv_usec = 0;

            int ret = select( camlist[idx_cfd]->cfd.fd + 1,
                              &fds, NULL, NULL, &tv );
        }

        int ret = v4l_buffer_dqbuf( &camlist[idx_cfd]->cfd,
                                    &camlist[idx_cfd]->vbuffer );
        if ( ret < 0 )
        {
            id_error = -1030;
            sprintf( str_error,
                     "device not able to dqbuf().");

            return false;
        }

        unsigned vbufflen = \
            v4l_queue_g_length( &camlist[idx_cfd]->vqueue, 0 );

        if ( vbufflen > 0 )
        {
            unsigned buffidx = camlist[idx_cfd]->vbuffer.buf.index;

            void* ptrData = \
                v4l_queue_g_mmapping( &camlist[idx_cfd]->vqueue,
                                      buffidx,
                                      0 );
            if ( ptrData != NULL )
            {
                bufflen = img_w * img_h * img_d;

                if( buff == NULL )
                    buff = new unsigned char[ bufflen ];

                if ( buff != NULL )
                {
                    memset( buff, 0, bufflen );
                    memcpy( buff , ptrData, vbufflen );

                    bufflen = vbufflen;

                    if ( ts != NULL )
                    {
                        const struct timeval* tv = \
                            v4l_buffer_g_timestamp( &camlist[idx_cfd]->vbuffer );

                        if ( tv != NULL )
                        {
                            unsigned ret_ts = 0;
                            ret_ts  = tv->tv_sec * 1000;
                            ret_ts += tv->tv_usec / 1000;

                            if ( ret_ts == 0 )
                            {
                                id_error = -500;
                                sprintf( str_error,
                                         "device time check error" );

                                delete[] buff;
                                buff = NULL;
                                bufflen = 0;
                            }

                            *ts = ret_ts;
                        }
                        else
                        {
                            id_error = -501;
                            sprintf( str_error,
                                     "device has no time" );
                            delete[] buff;
                            buff = NULL;
                            bufflen = 0;
                        }
                    }
                }
                else
                {
                    bufflen = 0;
                }
            }
        }


        ret = v4l_buffer_qbuf( &camlist[idx_cfd]->cfd,
                               &camlist[idx_cfd]->vbuffer );
        if( ret < 0 )
            return false;

        if ( bufflen > 0 )
            return true;
    }
    else
    {
        id_error = -1000;
        sprintf( str_error,
                 "device not open yet" );
    }

    return false;
}

void CamV4L2::wastebuffer( unsigned frames )
{
    if ( frames == 0 )
        frames = 1;

    if ( idx_cfd >= 0 )
    {
        if ( state_poll == false )
        {
            id_error = -1010;
            sprintf( str_error,
                     "device not stated at polling yet" );
            return;
        }

        for( unsigned cnt=0; cnt<frames; cnt++ )
        {
            int ret = 0;
#ifdef WASTEBUFFER_SYNC
            fd_set fds;
            struct timeval tv;

            FD_ZERO( &fds );
            FD_SET( camlist[idx_cfd]->cfd.fd, &fds );
            tv.tv_sec = 0;
            tv.tv_usec = 250 * 1000;

            ret = select( camlist[idx_cfd]->cfd.fd + 1,
                          &fds, NULL, NULL, &tv );


            struct pollfd plfd = {0};
            plfd.fd     = camlist[idx_cfd]->cfd.fd;
            plfd.events = POLLIN | POLLPRI;
            ret = ::poll( &plfd, 1, 26 );
#endif /// of WASTEBUFFER_SYNC

            ret = v4l_buffer_dqbuf( &camlist[idx_cfd]->cfd,
                                    &camlist[idx_cfd]->vbuffer );

            unsigned bufflen = \
                v4l_queue_g_length( &camlist[idx_cfd]->vqueue, 0 );
            if ( bufflen > 0 )
            {
                usleep( 1 );
            }

            ret = v4l_buffer_qbuf( &camlist[idx_cfd]->cfd,
                                   &camlist[idx_cfd]->vbuffer );
        }
    }
    else
    {
        id_error = -1000;
        sprintf( str_error,
                 "device not open yet" );
    }
}

void CamV4L2::clear_errors()
{
    id_error = 0;
    memset( str_error, 0 , 1024 );
}

void CamV4L2::map_devicefd()
{
    if ( idx_cfd < 0 )
    {
        // --
        // about devid and subdevid.
        // readid is /dev/video*
        // subdevid is /dev/v4l-subdev* for ioctl().
        // these indexing depends on what your linux system.
        // --
        unsigned real_devid    = id_req_dev * idx_fddiv;
        unsigned real_subdevid = (id_req_dev * 3) + 2;
        sprintf( str_devmap,    "/dev/video%u",      real_devid );
        sprintf( str_subdevmap, "/dev/v4l-subdev%u", real_subdevid );
    }
}

void CamV4L2::remove_ext_controls()
{
    if ( idx_cfd >= 0 )
    {
        if ( camlist[idx_cfd]->extCtlExposure != NULL )
        {
            delete camlist[idx_cfd]->extCtlExposure;
            camlist[idx_cfd]->extCtlExposure = NULL;
        }

        if ( camlist[idx_cfd]->extCtlGain != NULL )
        {
            delete camlist[idx_cfd]->extCtlGain;
            camlist[idx_cfd]->extCtlGain = NULL;
        }
    }
}

void CamV4L2::gain( unsigned v )
{
    if ( idx_cfd < 0 )
        return;

    if ( camlist[idx_cfd]->extCtlGain != NULL )
    {
        struct v4l2_control c = {0};

        int  maxv = camlist[idx_cfd]->extCtlGain->maximum;
        int  minv = camlist[idx_cfd]->extCtlGain->minimum;

        if ( v < camlist[idx_cfd]->extCtlGain->minimum )
        {
            v = camlist[idx_cfd]->extCtlGain->minimum;
        }
        else
        if ( v > camlist[idx_cfd]->extCtlGain->maximum )
        {
            v = camlist[idx_cfd]->extCtlGain->maximum;
        }

        c.id    = V4L2_CID_GAIN;
        c.value = v * ( v / camlist[idx_cfd]->extCtlGain->step );
        int ret = v4l_ioctl( &camlist[idx_cfd]->cfd, VIDIOC_S_CTRL, &c );

        if ( ret < 0 )
        {
            id_error = -200;
            sprintf( str_error,
                     "gain control failied" );
        }
        else
        {
#ifdef DEBUG
            printf( "Gain controlled : %X:%d/%d\n",
                    camlist[idx_cfd]->extCtlGain->id,
                    v,
                    camlist[idx_cfd]->extCtlGain->maximum );
#endif /// of DEBUG
        }
    }
}

unsigned CamV4L2::gain()
{
    if ( idx_cfd < 0 )
        return 0;

    if ( camlist[idx_cfd]->extCtlGain != NULL )
    {
        struct v4l2_control c = {0};
        c.id = V4L2_CID_GAIN;
        int ret = v4l_ioctl( &camlist[idx_cfd]->cfd, VIDIOC_G_CTRL, &c );
        if ( ret >= 0 )
        {
            return c.value;
        }
    }

    return 0;
}

void CamV4L2::gainrange( unsigned &minv, unsigned &maxv )
{
    if ( idx_cfd < 0 )
        return;

    if ( camlist[idx_cfd]->extCtlGain != NULL )
    {
        maxv = camlist[idx_cfd]->extCtlGain->maximum;
        minv = camlist[idx_cfd]->extCtlGain->minimum;
#ifdef DEBUG
        printf( "gain min, max = %u : %u \n", minv, maxv );
#endif
    }
}

void CamV4L2::exposure( unsigned v )
{
    if ( idx_cfd < 0 )
        return;

    if ( camlist[idx_cfd]->extCtlExposure != NULL )
    {
        struct v4l2_control c = {0};

        int  maxv = camlist[idx_cfd]->extCtlExposure->maximum;
        int  minv = camlist[idx_cfd]->extCtlExposure->minimum;

        if ( v < camlist[idx_cfd]->extCtlExposure->minimum )
        {
            v = camlist[idx_cfd]->extCtlExposure->minimum;
        }
        else
        if ( v > camlist[idx_cfd]->extCtlExposure->maximum )
        {
            v = camlist[idx_cfd]->extCtlExposure->maximum;
        }

        c.id    = V4L2_CID_EXPOSURE;
        c.value = camlist[idx_cfd]->extCtlExposure->step *
                  ( v / camlist[idx_cfd]->extCtlExposure->step );
        int ret = v4l_ioctl( &camlist[idx_cfd]->cfd, VIDIOC_S_CTRL, &c );

        if ( ret < 0 )
        {
            id_error = -210;
            sprintf( str_error,
                     "exposure control failied" );
        }
    }
}

unsigned CamV4L2::exposure()
{
     if ( idx_cfd < 0 )
        return 0;

    if ( camlist[idx_cfd]->extCtlGain != NULL )
    {
        struct v4l2_control c = {0};
        c.id = V4L2_CID_EXPOSURE;
        int ret = v4l_ioctl( &camlist[idx_cfd]->cfd, VIDIOC_G_CTRL, &c );
        if ( ret >= 0 )
        {
            return c.value;
        }
    }

    return 0;
}

void CamV4L2::exposurerange( unsigned &minv, unsigned &maxv )
{
    if ( idx_cfd < 0 )
    {
        return;
    }

    if ( camlist[idx_cfd]->extCtlExposure != NULL )
    {
        maxv = camlist[idx_cfd]->extCtlExposure->maximum;
        minv = camlist[idx_cfd]->extCtlExposure->minimum;
    }
}

float CamV4L2::temperature()
{
    // not implemented - 
    return 0.0f;
}

bool CamV4L2::framerate( float fps )
{
    // not implemented -

    return false;
}

float CamV4L2::framerate()
{
    // not implemented -
    return 0.f;
}

void CamV4L2::opendevice()
{
    if ( idx_cfd < 0 )
    {
        map_devicefd();

#ifdef DEBUG
        printf( "device mapped : %s\n", str_devmap );
        printf( "                %s\n", str_subdevmap );
        fflush( stdout );
#endif

        // check device existed.
        if ( access( str_devmap, 0 ) != 0 )
            return;

        caminfo* newcam = new caminfo();

        if ( newcam == NULL )
            return;

        memset( newcam, 0, sizeof( caminfo ) );
        newcam->sfd = -1;

#ifdef DEBUG
        printf( "initializing V4L_FD.\n" );
        fflush( stdout );
#endif
        v4l_fd_init( &newcam->cfd );

        int realfd = v4l_open( &newcam->cfd, str_devmap, true );
        if ( realfd < 0 )
        {
            delete newcam;
            return;
        }

#ifdef DEBUG
        printf( "device open, real fd = %d\n", newcam->cfd.fd );
#endif

        // open subdev for some ioctl().
        if ( access( str_subdevmap, 0 ) == 0 )
        {
            newcam->sfd = open( str_subdevmap,
                                O_RDWR | O_NONBLOCK,
                                0 );
        }

        struct v4l2_format vfmt = {0};
        unsigned captype = v4l_determine_type( &newcam->cfd );
        unsigned memtype = V4L2_MEMORY_MMAP;

        v4l_g_fmt( &newcam->cfd, &vfmt, captype );
        
        // try to MJPEG if should able.
        v4l_format_s_pixelformat( &vfmt, V4L2_PIX_FMT_MJPEG );
        if ( v4l_try_fmt( &newcam->cfd, &vfmt, false ) == 0 )
        {
            v4l_s_fmt( &newcam->cfd, &vfmt, false );
        }
        v4l_g_fmt( &newcam->cfd, &vfmt, captype );

        img_f = v4l_format_g_pixelformat( &vfmt );
        img_p = v4l_format_g_num_planes( &vfmt );
        img_w = v4l_format_g_width( &vfmt );
        img_h = v4l_format_g_height( &vfmt );

#ifdef DEBUG        
        printf( "current format : %s\n", fmt2str( img_f ) );
#endif

        newcam->bufferGrab = new unsigned char[ img_w * img_h * img_d ];
        if ( newcam->bufferGrab == NULL )
        {
            id_error = -110;
            sprintf( str_error,
                     "cannot allocate render buffer.\n" );
            v4l_close( &newcam->cfd );
            delete newcam;
            return;
        }
#ifdef MEM_DEBUG
        else
        {
            printf( "! newcam->bufferGrab allocated at 0x%X ( %u )\n",
                    newcam->bufferGrab, img_w * img_h * img_d );
        }
#endif

#ifdef DEBUG
        printf( "Enumerating ext.controls...\n" );
        fflush( stdout );
#endif
        get_ext_controls( (void*)newcam );

        // insert myself in empty slot.
        for( idx_cfd=0; idx_cfd<4; idx_cfd++ )
        {
            if ( camlist[idx_cfd] == NULL )
            {
                camlist[idx_cfd] = newcam;
                break;
            }
        }

#ifdef DEBUG
        printf( "idx_cfd = %d\n", idx_cfd );
        printf( "Triggering event of controlled ..\n" );
        fflush( stdout );
#endif

        if ( eventhandler != NULL )
        {
            eventhandler->controlled( CamEvent::CSIG_OPEN,
                                      &str_devmap );
        }
    }
}

void CamV4L2::closedevice()
{
    if ( idx_cfd >= 0 )
    {
        stop();

        deallocateBuffers();

        caminfo* ci = camlist[idx_cfd];

        if ( ci == NULL )
            return;

        v4l_close( &ci->cfd );

        if ( ci->sfd >= 0 )
        {
            close( ci->sfd );
            ci->sfd = -1;
        }

        if ( ci->bufferGrab != NULL )
        {
            delete[] ci->bufferGrab;
            ci->bufferGrab = NULL;
        }

        remove_ext_controls();

        camlist[idx_cfd] = NULL;

        idx_cfd = -1;
        delete ci;

        if ( eventhandler != NULL )
        {
            eventhandler->controlled( CamEvent::CSIG_CLOSE, NULL );
        }
    }
}

void CamV4L2::get_ext_controls( void* p )
{
    caminfo* pcam = (caminfo*)p;

    if ( pcam == NULL )
        return;

    // find exposure control .
    struct v4l2_query_ext_ctrl* pec = new struct v4l2_query_ext_ctrl;

    memset( pec, 0, sizeof( struct v4l2_query_ext_ctrl ) );

    pec->id = V4L2_CID_EXPOSURE;

    int ret = v4l_query_ext_ctrl( &pcam->cfd,
                                  pec,
                                  false,
                                  false );
    if ( ret >= 0 )
    {
        if ( pcam->extCtlExposure == NULL )
        {
            pcam->extCtlExposure = pec;
        }
        else
        {
            delete pec;
        }
    }

    // then find gain control.
    pec = new struct v4l2_query_ext_ctrl;

    memset( pec, 0, sizeof( struct v4l2_query_ext_ctrl ) );

    pec->id = V4L2_CID_EXPOSURE;

    ret = v4l_query_ext_ctrl( &pcam->cfd,
                              pec, true, false );
    if ( ret >= 0 )
    {
        if ( pcam->extCtlGain == NULL )
        {
            pcam->extCtlGain = pec;
        }
        else
        {
            delete pec;
        }
    }
}

bool CamV4L2::allocateBuffers()
{
    int ret = 0;
    unsigned captype = v4l_determine_type( &camlist[idx_cfd]->cfd );
    unsigned memtype = V4L2_MEMORY_MMAP;

#ifdef DEBUG
    printf( "Preparing V4L2 buffer request ..\n" );
    fflush( stdout );
#endif

    v4l_buffer_init( &camlist[idx_cfd]->vbuffer, captype, memtype, 0 );
    ret = v4l_buffer_prepare_buf( &camlist[idx_cfd]->cfd,
                                  &camlist[idx_cfd]->vbuffer );
    if ( ret < 0 )
    {
        id_error = -101;
        sprintf( str_error,
                  "V4L2 preparing buffer failed" );
        return false;
    }

#ifdef DEBUG
    printf( "Initializing V4L2 queue ..\n" );
    fflush( stdout );
#endif
    v4l_queue_init( &camlist[idx_cfd]->vqueue, captype, memtype );

    ret = v4l_queue_reqbufs( &camlist[idx_cfd]->cfd,
                             &camlist[idx_cfd]->vqueue,
                             REQ_BUFF_SZ );
    if ( ret < 0 )
    {
        id_error = -102;
        sprintf( str_error,
                 "V4L requesting buffers failed" );
        return false;
    }

#ifdef DEBUG
    printf( "Requesting V4L2 obtain buffers ..\n" );
    fflush( stdout );
#endif
    ret = v4l_queue_obtain_bufs( &camlist[idx_cfd]->cfd,
                                 &camlist[idx_cfd]->vqueue,
                                 0 );
    if ( ret < 0 )
    {
        id_error = -103;
        sprintf( str_error,
                 "V4L queue request buffer failed" );
        return false;
    }

#ifdef DEBUG
    printf( "Requesting V4L2 round initializing buffers ..\n" );
    fflush( stdout );
#endif
    unsigned buffersz = v4l_queue_g_buffers( &camlist[idx_cfd]->vqueue );

    for( unsigned cnt=0; cnt<buffersz; cnt++ )
    {
        v4l_queue_buffer_init( &camlist[idx_cfd]->vqueue,
                               &camlist[idx_cfd]->vbuffer,
                               cnt );

        ret = v4l_buffer_qbuf( &camlist[idx_cfd]->cfd,
                               &camlist[idx_cfd]->vbuffer );
        if ( ret < 0 )
        {
            id_error = -104;
            sprintf( str_error,
                     "V4L queue buffer queuing failed" );
            return false;
        }
    }

        return true;
}

bool CamV4L2::deallocateBuffers()
{
    v4l_queue_free( &camlist[idx_cfd]->cfd,
                    &camlist[idx_cfd]->vqueue );

    v4l_queue_munmap_bufs( &camlist[idx_cfd]->cfd,
                           &camlist[idx_cfd]->vqueue,
                           REQ_BUFF_SZ );

    return true;
}
#endif /// of _WIN32
