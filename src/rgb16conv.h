#ifndef __RGB16CONV_H__
#define __RGB16CONV_H__

unsigned rgb555rgb(void* in, unsigned sz, void** out,unsigned width, unsigned height );
unsigned rgb565rgb(void* in, unsigned sz, void** out,unsigned width, unsigned height );

#endif // __RGB16CONV_H__

