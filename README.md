# l4v2camon

A small project for Embedded Linux camera (v4l2) control service daemon.

## Works on
* Linux ( x86, arm ) only

## Works with
* Almost device with V4L2
	1. MIPI-CSI2 cameras
	1. UVC (USB Video Class )
	1. more

## Requires
* v4l-utils ( need source at [linuxtv](http://git.linuxtv.org/v4l-utils.git ). )
* [FLTK-1.3.5-ts](https://github.com/rageworx/fltk-1.3.5-2-ts)
* [fl_imgtk](https://github.com/rageworx/fl_imgtk)

## Supported formats
* YUV ( YUYV, YUVY )
* MJPEG
* BMP ( Windows format )

## Tested on
* Raspberry Pi 3B+
* Rockchip RK3399 ( Rock960B/C, Saphire Excavator )
* Linux Mint 18.x ( with USB camera )

## Icon author
* https://www.flaticon.com/authors/pixel-buddha
